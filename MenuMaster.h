/**
 *@file MenuMaster.h
 *@brief declaration of the class that handles the menu.
 *@details This class is responsible for creating the menu. It interacts with almost all of the other objects including
 *the Display objects, the keyboard, and the simulator objects. The menu allows players to select from a variety of game modes and then start a game.
 *Inherits publicly from Simulator.
 *@author Joshua Vandenhoek, Eric DenHaan, Shaun Cullen, Lance Chisholm
 *@bug after a game ends, the menu does not pop back up so that we can play more.
 */
 

#ifndef MENUMASTER_H
#define MENUMASTER_H

#include "Drawable.h"
#include "Updateable.h"
#include "Simulator.h"
#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <iostream>
#include <string>
#include <sstream>
#include "Point.h"

class MenuMaster : public Simulator
{
  private:
   ALLEGRO_DISPLAY* disp; /**< private data member that is an allegro display pointer. needed so that we can display the menu and then play the game.*/
   ALLEGRO_FONT *fon; /**< private data member that is responsible for storing the font of the text that appears in the menu.*/
   Point title = Point(400,100); /**< private data member that is the origin point of the title text, displays it in the middle of the screen.*/
   Point opt1 = Point(400,250); /**< private data member that is the origin point of the option 1 text on the screen.*/
   Point opt2 = Point(400,400); /**< private data member that is the origin point of the option 2 text on the screen.*/
   bool oneKeyPress=false, twoKeyPress=false; /**< boolean private data members that describe whether the "1" or "2" key has been pressed, indicating what mode to start in.*/
   std::string t = "Poing"; /**< string private data member that contains the text for the title.*/
   std::string o1 = "1.Human vs. Human"; /**< string private data member that contains the text for option 1.*/
   std::string o2 = "2.Human vs. Computer"; /**< string provate data member that contains the text for option 2.*/
   int mode; /**< integer private data member that denotes what mode has been selected.*/
   

   int width, height; /**< integer private data member that defines the widht and height of the display object that thte menu uses.*/
   
  public:
   //constructor
   /**
    *@fn MenuMaster(const Display& d, Audio& a, int fps) : Simulator(d,a,fps)
    *@brief Constructor for the menu object
    *@details the constructor takes in a Display object, an Audio object, and a frames per second parameter. Sets its data members disp, width, and height.
    *initializes and loads the necessary allegro font objects. 
    *@param d const Display reference that represents the display. Menu needs this object to be seen in the window.
    *@param a an Audio reference object that will be responsible for audio playing in the menu.
    *@param fps an integer that sets the frames per second that the window will display at.
    *@return a menu object.
    */
  MenuMaster(const Display& d, Audio& a, int fps) : Simulator(d,a,fps)
   {
      disp = d.getAllegroDisplay();
      width = d.getW();
      height = d.getH();
      
      al_init_font_addon();
      al_init_ttf_addon();
      fon = al_load_ttf_font("/usr/share/fonts/dejavu/DejaVuSerif.ttf", 24, 0);
   


   }  
   ///Dummy implementation in this class for this function. The menu does not move.
   void move(double);
   ///Dummy implementation in this class for this function. Drawing is handled in drawModel().
   void draw();
   ///Dummy implementation in this class for this function. No updating on screen happens.
   void updateModel(double);
   /**
    *@fn void drawModel()
    *@brief function that is responsible for drawing the menu on screen.
    *@details This function utilizes string streams to convert the strings that have the title text and option 1 & 2 text.
    *Utilizes a built in allegro function to display the text to the screen.
    *@param none.
    *@return void.
    */
   void drawModel();
   /**
    *@fn void playerinput (int a, bool pressrelease)
    *@brief this function detects player input and uses the input to select a mode.
    *@details this function takes an int that represents the type of key that is pressed and a boolean variable that is detecting
    *whether a key has been pressed or released. If it is the case that a key has been pressed, then we determine what key it was and set
    *the game mode accordingly.
    *@param a an integer that represents the key code when a keyboard key is pressed.
    *@param pressrelease a boolean variable that tells whether a key has been pressed or released.
    *@return void.
    */
   void playerinput(int, bool);
   ///Dummy implementation for this function  there is no collision that occurs in the menu
   void collision(double);
   /**
    *@fn int runMenu()
    *@brief function responsible for performing all menu functions.
    *@details this function generates its own event queue in order to wait for input from the user.This function also draws the menu on the screen.
    *When the user presses a key, the function then detects what key was pressed and returns the mode to the main game.
    *This will be used by the main game to initialize the appropariate game mode.
    *@param none.
    *@return an int that represents the game mode that should be initialized.
    */

   int runMenu();
   
};
#endif
