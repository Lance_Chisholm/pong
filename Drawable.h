/**
 *@file Drawable.h
 *@brief Drawable class.
 *@details Pure Virtual Drawable class.
 *@authors Lance CHisholm.
 *@bug None.
 */
#ifndef DRAWABLE_H
#define DRAWABLE_H

class Drawable {
  public:
   /**
    *@fn draw
    *@brief Draw function.
    *@details Virtual Draw function, no implementation provided in this class.
    *@param none.
    *@return void.
    */
   virtual void draw() =0;
};

#endif
