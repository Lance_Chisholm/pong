/**
 *@file ScreenMaster.h
 *@brief Stores the lists of objects for our game.
 *@details Handles everything from collision to scorring, to endgame. This Class could be though of as the engine of the game.
 *@author Lance Chisholm, Eric DenHaan, Shaun Cullen, Joshua Vandenhoek.
 *@bug we were not smart about screen size, so if we construct a screen size other than 800x600, the game will not look correct. 
 */
#ifndef SCREENMASTER_H
#define SCREENMASTER_H

#include "Simulator.h"
#include <allegro5/allegro_primitives.h>
#include <memory>
#include <list>
#include <iostream>
#include "Audio.h"
#include "PlayerPaddle.h"
#include "ComputerPaddle.h"
#include "Paddle.h"
#include "Updateable.h"
#include "Drawable.h"
#include "Ball.h"
#include "Score.h"
#include "Net.h"

//Using one simulator class that works with multiple objects of different type.
class ScreenMaster: public Simulator, public Audio {
   int width, height;/**<Data members the represent the Dimensions of the screen.*/
   ALLEGRO_FONT* fon = al_load_ttf_font("pong_fonts/wendy.ttf", 50, 0); /**<Pointer to the font this class is using.*/
   //keep a list of updateable and drawables
   std::list <std::shared_ptr<Updateable>> ULIST;/**<Updateable list of objects, any objec that can be moved will be in this list.*/
   std::list <std::shared_ptr<Drawable>> DLIST;/**<Drawable list of objects, every object that needs to be seen by the user wil lbe in this list.*/
   std::list <std::shared_ptr<Paddle>> PADDLE_LIST;/**<List of paddles (both player and computer), that are are on the screen*/
   std::list <std::shared_ptr<Ball>> BALL_LIST;/**<List of all Balls on the screen.*/
   std::shared_ptr<Score> ScoreOne = std::make_shared<Score>(500,25);/**<Object pointer to the the first score.*/
   std::shared_ptr<Score> ScoreTwo = std::make_shared<Score>(250,25);/**<Object pointer to the the second score.*/

   std::shared_ptr<PlayerPaddle> pPointer2 =
      std::make_shared<PlayerPaddle>(20.0, 100.0, 150.0, 400.0, ALLEGRO_KEY_W, ALLEGRO_KEY_S);/**<Second player paddle, controlled by W and S.*/

   std::shared_ptr<PlayerPaddle> pPointer =
      std::make_shared<PlayerPaddle>(20.0, 100.0, 650.0, 400.0, ALLEGRO_KEY_UP, ALLEGRO_KEY_DOWN);/**<First player paddle, controlled by the up and down arrows.*/

   
   std::shared_ptr<ComputerPaddle> CPUPointer = std::make_shared<ComputerPaddle>(20.0, 100.0, 150.0, 300.0);/**<Computer paddle pointer.*/
   std::shared_ptr<Net> leftNetUp = std::make_shared<Net>(20.0, 200.0, 795.0, 50.0);/**<Top left Goalpost.*/
   std::shared_ptr<Net> leftNetDown = std::make_shared<Net>(20.0, 200.0,795.0, 550.0);/**<Bottom left Goalpost.*/
   std::shared_ptr<Net> rightNetUp = std::make_shared<Net>(20.0, 200.0, 5.0, 50.0);/**<Top right Goalpost.*/
   std::shared_ptr<Net> rightNetDown = std::make_shared<Net>(20.0, 200.0, 5.0, 550.0);/**<Bottom right Goalpost.*/
   
  
public:
   //this paddlepointer is so that ScreenMaster can communicate with the paddle
   /**
    *@fn ScreenMaster(const Display & d, const Audio & a, int fps) : Simulator(d, a, fps)
    *@brief ScreenMaster Constructor.
    *@details Calls the Simulator Constructor and sets the width and height of the screen to be the same size as the display object being passed to it.
    *@param d is a const Display object, a is a const audio object, fps is the frames per second.
    *@return Retruns a ScreenMaster object.
    */
  ScreenMaster(const Display & d, const Audio & a, int fps) : Simulator(d, a, fps)
   {
      width = d.getW();
      height = d.getH();
   }

   /**
    *@fn addUpdateable(std::shared_ptr<Updateable>);
    *@brief Adds a Updateable object to the Updateable list.
    *@details Adds a Updateable object to the Updateable list.
    *@param An shared_ptr to an updateable object to be added to the updateable list.
    *@return void.
    */
   void addUpdateable(std::shared_ptr<Updateable>);

   /**
    *@fn addDrawable(std::shared_ptr<Drawable>);
    *@brief Adds a Drawable object to the Drawable list.
    *@details Adds a Drawable object to the Drawable list.
    *@param An shared_ptr to a Drawable object to be added to the Drawable list.
    *@return void.
    */
   void addDrawable(std::shared_ptr<Drawable>);

   /**
    *@fn addPaddle(std::shared_ptr<Paddle>);
    *@brief Adds a Paddle object to the Paddle list.
    *@details Adds a Paddle object to the Paddle list.
    *@param An shared_ptr to a paddle object to be added to the paddle list.
    *@return void.
    */
   void addPaddle(std::shared_ptr<Paddle>);

   /**
    *@fn addCPUPaddle(std::shared_ptr<Paddle>);
    *@brief Adds a Paddle object to the CPUPaddle list.
    *@details Adds a Paddle object to the CPUPaddle list.
    *@param An shared_ptr to a paddle object to be added to the CPUpaddle list.
    *@return void.
    */
   void addCPUPaddle(std::shared_ptr<Paddle>);

   /**
    *@fn addBall(std::shared_ptr<Ball>);
    *@brief Adds a Ball object to the Ball list.
    *@details Adds a Ball object to the Ball list.
    *@param An shared_ptr to a Ball object to be added to the Ball list.
    *@return void.
    */
   void addBall(std::shared_ptr<Ball>);

   /**
    *@fn updateModel(double dt);
    *@brief Updates positions of objects, handles score and win conditions.
    *@details Iterates through the ball list and checks the collision of each ball with every paddle. Checks the position of every ball and scores based of position of each ball.
    *Resets the ball position if a score occurs. Ends the game and draws win message upon a win.
    *@param dt is the current runtime of the allegro event queue.
    *@return void.
    */
   void updateModel(double dt);

   /**
    *@fn drawModel
    *@brief Draws all of the objects to the screen.
    *@details Iterates through the draw list and draws every game object in the drawable list.
    *@param none.
    *@return void.
    */
   void drawModel();  

   /**
    *@fn playerinput(int a, bool pressrelease);
    *@brief gets the code of the key press.
    *@details Handles the player input keypresses.
    *@param a is the ALLEGRO key code, pressrelease is a boolean value that flags if a key is beinging pressed or not.
    *@return void.
    */
   void playerinput(int a, bool pressrelease);

   // set up right human player versus left computer player
   /**
    *@fn initialize3
    *@brief Computer vs player
    *@details Sets up a game with a single ball for computer vs player mode.
    *@param none.
    *@return void.
    */
   void initialize3();

   /**
    *@fn initialize
    *@brief Sets up a Human vs Human game.
    *@details Sets up the objects for a human vs hooman game with a single ball. 
    *@param none.
    *@return void.
    */
   void initialize();

   /**
    *@fn collision(double td);
    *@brief Checks collisions between Balls and paddles.
    *@details Collisions are handled between objects in this function.
    *@param dt is the runtime of the program.
    *@return void.
    */
   void collision(double dt);

};

#endif
	 
