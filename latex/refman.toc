\contentsline {chapter}{\numberline {1}Bug List}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Class Index}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Class Hierarchy}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}Class Index}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Class List}{5}{section.3.1}
\contentsline {chapter}{\numberline {4}File Index}{7}{chapter.4}
\contentsline {section}{\numberline {4.1}File List}{7}{section.4.1}
\contentsline {chapter}{\numberline {5}Class Documentation}{9}{chapter.5}
\contentsline {section}{\numberline {5.1}Audio Class Reference}{9}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Constructor \& Destructor Documentation}{10}{subsection.5.1.1}
\contentsline {subsubsection}{\numberline {5.1.1.1}Audio}{10}{subsubsection.5.1.1.1}
\contentsline {subsubsection}{\numberline {5.1.1.2}$\sim $Audio}{10}{subsubsection.5.1.1.2}
\contentsline {subsection}{\numberline {5.1.2}Member Function Documentation}{10}{subsection.5.1.2}
\contentsline {subsubsection}{\numberline {5.1.2.1}samplesLoaded}{10}{subsubsection.5.1.2.1}
\contentsline {subsection}{\numberline {5.1.3}Member Data Documentation}{10}{subsection.5.1.3}
\contentsline {subsubsection}{\numberline {5.1.3.1}bkgrnd\_\discretionary {-}{}{}music}{10}{subsubsection.5.1.3.1}
\contentsline {subsubsection}{\numberline {5.1.3.2}bkgrnd\_\discretionary {-}{}{}music\_\discretionary {-}{}{}inst}{10}{subsubsection.5.1.3.2}
\contentsline {subsubsection}{\numberline {5.1.3.3}p\_\discretionary {-}{}{}hit}{10}{subsubsection.5.1.3.3}
\contentsline {subsubsection}{\numberline {5.1.3.4}p\_\discretionary {-}{}{}hit\_\discretionary {-}{}{}inst}{10}{subsubsection.5.1.3.4}
\contentsline {subsubsection}{\numberline {5.1.3.5}score}{11}{subsubsection.5.1.3.5}
\contentsline {subsubsection}{\numberline {5.1.3.6}score\_\discretionary {-}{}{}inst}{11}{subsubsection.5.1.3.6}
\contentsline {subsubsection}{\numberline {5.1.3.7}w\_\discretionary {-}{}{}hit}{11}{subsubsection.5.1.3.7}
\contentsline {subsubsection}{\numberline {5.1.3.8}w\_\discretionary {-}{}{}hit\_\discretionary {-}{}{}inst}{11}{subsubsection.5.1.3.8}
\contentsline {section}{\numberline {5.2}Ball Class Reference}{12}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Constructor \& Destructor Documentation}{13}{subsection.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.1.1}Ball}{13}{subsubsection.5.2.1.1}
\contentsline {subsection}{\numberline {5.2.2}Member Function Documentation}{13}{subsection.5.2.2}
\contentsline {subsubsection}{\numberline {5.2.2.1}collisionCheck}{13}{subsubsection.5.2.2.1}
\contentsline {subsubsection}{\numberline {5.2.2.2}draw}{13}{subsubsection.5.2.2.2}
\contentsline {subsubsection}{\numberline {5.2.2.3}getBallSpeed}{14}{subsubsection.5.2.2.3}
\contentsline {subsubsection}{\numberline {5.2.2.4}GetPos}{14}{subsubsection.5.2.2.4}
\contentsline {subsubsection}{\numberline {5.2.2.5}getRadius}{14}{subsubsection.5.2.2.5}
\contentsline {subsubsection}{\numberline {5.2.2.6}HorzBounce}{14}{subsubsection.5.2.2.6}
\contentsline {subsubsection}{\numberline {5.2.2.7}move}{15}{subsubsection.5.2.2.7}
\contentsline {subsubsection}{\numberline {5.2.2.8}resetBall}{15}{subsubsection.5.2.2.8}
\contentsline {subsubsection}{\numberline {5.2.2.9}samplesLoaded}{15}{subsubsection.5.2.2.9}
\contentsline {subsubsection}{\numberline {5.2.2.10}VertBounce}{16}{subsubsection.5.2.2.10}
\contentsline {subsection}{\numberline {5.2.3}Member Data Documentation}{16}{subsection.5.2.3}
\contentsline {subsubsection}{\numberline {5.2.3.1}bkgrnd\_\discretionary {-}{}{}music\_\discretionary {-}{}{}inst}{16}{subsubsection.5.2.3.1}
\contentsline {subsubsection}{\numberline {5.2.3.2}buffer}{16}{subsubsection.5.2.3.2}
\contentsline {subsubsection}{\numberline {5.2.3.3}F}{16}{subsubsection.5.2.3.3}
\contentsline {subsubsection}{\numberline {5.2.3.4}maxSpeed}{16}{subsubsection.5.2.3.4}
\contentsline {subsubsection}{\numberline {5.2.3.5}minSpeed}{16}{subsubsection.5.2.3.5}
\contentsline {subsubsection}{\numberline {5.2.3.6}origin}{16}{subsubsection.5.2.3.6}
\contentsline {subsubsection}{\numberline {5.2.3.7}p\_\discretionary {-}{}{}hit\_\discretionary {-}{}{}inst}{16}{subsubsection.5.2.3.7}
\contentsline {subsubsection}{\numberline {5.2.3.8}radius}{16}{subsubsection.5.2.3.8}
\contentsline {subsubsection}{\numberline {5.2.3.9}score\_\discretionary {-}{}{}inst}{16}{subsubsection.5.2.3.9}
\contentsline {subsubsection}{\numberline {5.2.3.10}speed}{16}{subsubsection.5.2.3.10}
\contentsline {subsubsection}{\numberline {5.2.3.11}timecheck}{17}{subsubsection.5.2.3.11}
\contentsline {subsubsection}{\numberline {5.2.3.12}timeE}{17}{subsubsection.5.2.3.12}
\contentsline {subsubsection}{\numberline {5.2.3.13}w\_\discretionary {-}{}{}hit\_\discretionary {-}{}{}inst}{17}{subsubsection.5.2.3.13}
\contentsline {section}{\numberline {5.3}ComputerPaddle Class Reference}{18}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Constructor \& Destructor Documentation}{19}{subsection.5.3.1}
\contentsline {subsubsection}{\numberline {5.3.1.1}ComputerPaddle}{19}{subsubsection.5.3.1.1}
\contentsline {subsection}{\numberline {5.3.2}Member Function Documentation}{19}{subsection.5.3.2}
\contentsline {subsubsection}{\numberline {5.3.2.1}BottomRight}{19}{subsubsection.5.3.2.1}
\contentsline {subsubsection}{\numberline {5.3.2.2}calcCornerPoints}{20}{subsubsection.5.3.2.2}
\contentsline {subsubsection}{\numberline {5.3.2.3}checkKey}{20}{subsubsection.5.3.2.3}
\contentsline {subsubsection}{\numberline {5.3.2.4}draw}{20}{subsubsection.5.3.2.4}
\contentsline {subsubsection}{\numberline {5.3.2.5}getBallPosition}{20}{subsubsection.5.3.2.5}
\contentsline {subsubsection}{\numberline {5.3.2.6}getPaddleSpeed}{20}{subsubsection.5.3.2.6}
\contentsline {subsubsection}{\numberline {5.3.2.7}getPos}{21}{subsubsection.5.3.2.7}
\contentsline {subsubsection}{\numberline {5.3.2.8}getSize}{21}{subsubsection.5.3.2.8}
\contentsline {subsubsection}{\numberline {5.3.2.9}move}{21}{subsubsection.5.3.2.9}
\contentsline {subsubsection}{\numberline {5.3.2.10}setDownFlag}{21}{subsubsection.5.3.2.10}
\contentsline {subsubsection}{\numberline {5.3.2.11}setUpFlag}{21}{subsubsection.5.3.2.11}
\contentsline {subsubsection}{\numberline {5.3.2.12}TopLeft}{22}{subsubsection.5.3.2.12}
\contentsline {subsection}{\numberline {5.3.3}Member Data Documentation}{22}{subsection.5.3.3}
\contentsline {subsubsection}{\numberline {5.3.3.1}bottomRightCorner}{22}{subsubsection.5.3.3.1}
\contentsline {subsubsection}{\numberline {5.3.3.2}currentBallPos}{22}{subsubsection.5.3.3.2}
\contentsline {subsubsection}{\numberline {5.3.3.3}elapsedTime}{22}{subsubsection.5.3.3.3}
\contentsline {subsubsection}{\numberline {5.3.3.4}origin}{22}{subsubsection.5.3.3.4}
\contentsline {subsubsection}{\numberline {5.3.3.5}paddleSpeed}{22}{subsubsection.5.3.3.5}
\contentsline {subsubsection}{\numberline {5.3.3.6}size}{22}{subsubsection.5.3.3.6}
\contentsline {subsubsection}{\numberline {5.3.3.7}topLeftCorner}{22}{subsubsection.5.3.3.7}
\contentsline {section}{\numberline {5.4}Display Class Reference}{23}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Detailed Description}{23}{subsection.5.4.1}
\contentsline {subsection}{\numberline {5.4.2}Constructor \& Destructor Documentation}{23}{subsection.5.4.2}
\contentsline {subsubsection}{\numberline {5.4.2.1}Display}{23}{subsubsection.5.4.2.1}
\contentsline {subsubsection}{\numberline {5.4.2.2}$\sim $Display}{24}{subsubsection.5.4.2.2}
\contentsline {subsection}{\numberline {5.4.3}Member Function Documentation}{24}{subsection.5.4.3}
\contentsline {subsubsection}{\numberline {5.4.3.1}getAllegroDisplay}{24}{subsubsection.5.4.3.1}
\contentsline {subsubsection}{\numberline {5.4.3.2}getH}{24}{subsubsection.5.4.3.2}
\contentsline {subsubsection}{\numberline {5.4.3.3}getW}{24}{subsubsection.5.4.3.3}
\contentsline {subsection}{\numberline {5.4.4}Member Data Documentation}{24}{subsection.5.4.4}
\contentsline {subsubsection}{\numberline {5.4.4.1}display}{24}{subsubsection.5.4.4.1}
\contentsline {subsubsection}{\numberline {5.4.4.2}height}{24}{subsubsection.5.4.4.2}
\contentsline {subsubsection}{\numberline {5.4.4.3}width}{24}{subsubsection.5.4.4.3}
\contentsline {section}{\numberline {5.5}Drawable Class Reference}{25}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Member Function Documentation}{25}{subsection.5.5.1}
\contentsline {subsubsection}{\numberline {5.5.1.1}draw}{25}{subsubsection.5.5.1.1}
\contentsline {section}{\numberline {5.6}MenuMaster Class Reference}{26}{section.5.6}
\contentsline {subsection}{\numberline {5.6.1}Constructor \& Destructor Documentation}{27}{subsection.5.6.1}
\contentsline {subsubsection}{\numberline {5.6.1.1}MenuMaster}{27}{subsubsection.5.6.1.1}
\contentsline {subsection}{\numberline {5.6.2}Member Function Documentation}{27}{subsection.5.6.2}
\contentsline {subsubsection}{\numberline {5.6.2.1}collision}{27}{subsubsection.5.6.2.1}
\contentsline {subsubsection}{\numberline {5.6.2.2}draw}{27}{subsubsection.5.6.2.2}
\contentsline {subsubsection}{\numberline {5.6.2.3}drawModel}{27}{subsubsection.5.6.2.3}
\contentsline {subsubsection}{\numberline {5.6.2.4}getQueue}{28}{subsubsection.5.6.2.4}
\contentsline {subsubsection}{\numberline {5.6.2.5}move}{28}{subsubsection.5.6.2.5}
\contentsline {subsubsection}{\numberline {5.6.2.6}playerinput}{28}{subsubsection.5.6.2.6}
\contentsline {subsubsection}{\numberline {5.6.2.7}run}{28}{subsubsection.5.6.2.7}
\contentsline {subsubsection}{\numberline {5.6.2.8}runMenu}{28}{subsubsection.5.6.2.8}
\contentsline {subsubsection}{\numberline {5.6.2.9}updateModel}{29}{subsubsection.5.6.2.9}
\contentsline {subsection}{\numberline {5.6.3}Member Data Documentation}{29}{subsection.5.6.3}
\contentsline {subsubsection}{\numberline {5.6.3.1}disp}{29}{subsubsection.5.6.3.1}
\contentsline {subsubsection}{\numberline {5.6.3.2}fon}{29}{subsubsection.5.6.3.2}
\contentsline {subsubsection}{\numberline {5.6.3.3}gameOver}{29}{subsubsection.5.6.3.3}
\contentsline {subsubsection}{\numberline {5.6.3.4}height}{29}{subsubsection.5.6.3.4}
\contentsline {subsubsection}{\numberline {5.6.3.5}mode}{29}{subsubsection.5.6.3.5}
\contentsline {subsubsection}{\numberline {5.6.3.6}o1}{29}{subsubsection.5.6.3.6}
\contentsline {subsubsection}{\numberline {5.6.3.7}o2}{29}{subsubsection.5.6.3.7}
\contentsline {subsubsection}{\numberline {5.6.3.8}oneKeyPress}{29}{subsubsection.5.6.3.8}
\contentsline {subsubsection}{\numberline {5.6.3.9}opt1}{29}{subsubsection.5.6.3.9}
\contentsline {subsubsection}{\numberline {5.6.3.10}opt2}{29}{subsubsection.5.6.3.10}
\contentsline {subsubsection}{\numberline {5.6.3.11}t}{29}{subsubsection.5.6.3.11}
\contentsline {subsubsection}{\numberline {5.6.3.12}title}{30}{subsubsection.5.6.3.12}
\contentsline {subsubsection}{\numberline {5.6.3.13}twoKeyPress}{30}{subsubsection.5.6.3.13}
\contentsline {subsubsection}{\numberline {5.6.3.14}width}{30}{subsubsection.5.6.3.14}
\contentsline {section}{\numberline {5.7}Net Class Reference}{31}{section.5.7}
\contentsline {subsection}{\numberline {5.7.1}Constructor \& Destructor Documentation}{32}{subsection.5.7.1}
\contentsline {subsubsection}{\numberline {5.7.1.1}Net}{32}{subsubsection.5.7.1.1}
\contentsline {subsection}{\numberline {5.7.2}Member Function Documentation}{32}{subsection.5.7.2}
\contentsline {subsubsection}{\numberline {5.7.2.1}BottomRight}{32}{subsubsection.5.7.2.1}
\contentsline {subsubsection}{\numberline {5.7.2.2}calcCornerPoints}{32}{subsubsection.5.7.2.2}
\contentsline {subsubsection}{\numberline {5.7.2.3}checkKey}{33}{subsubsection.5.7.2.3}
\contentsline {subsubsection}{\numberline {5.7.2.4}draw}{33}{subsubsection.5.7.2.4}
\contentsline {subsubsection}{\numberline {5.7.2.5}getBallPosition}{33}{subsubsection.5.7.2.5}
\contentsline {subsubsection}{\numberline {5.7.2.6}getPaddleSpeed}{33}{subsubsection.5.7.2.6}
\contentsline {subsubsection}{\numberline {5.7.2.7}getPos}{33}{subsubsection.5.7.2.7}
\contentsline {subsubsection}{\numberline {5.7.2.8}getSize}{34}{subsubsection.5.7.2.8}
\contentsline {subsubsection}{\numberline {5.7.2.9}move}{34}{subsubsection.5.7.2.9}
\contentsline {subsubsection}{\numberline {5.7.2.10}setDownFlag}{34}{subsubsection.5.7.2.10}
\contentsline {subsubsection}{\numberline {5.7.2.11}setUpFlag}{34}{subsubsection.5.7.2.11}
\contentsline {subsubsection}{\numberline {5.7.2.12}TopLeft}{34}{subsubsection.5.7.2.12}
\contentsline {subsection}{\numberline {5.7.3}Member Data Documentation}{35}{subsection.5.7.3}
\contentsline {subsubsection}{\numberline {5.7.3.1}bottomRightCorner}{35}{subsubsection.5.7.3.1}
\contentsline {subsubsection}{\numberline {5.7.3.2}elapsedTime}{35}{subsubsection.5.7.3.2}
\contentsline {subsubsection}{\numberline {5.7.3.3}origin}{35}{subsubsection.5.7.3.3}
\contentsline {subsubsection}{\numberline {5.7.3.4}paddleSpeed}{35}{subsubsection.5.7.3.4}
\contentsline {subsubsection}{\numberline {5.7.3.5}size}{35}{subsubsection.5.7.3.5}
\contentsline {subsubsection}{\numberline {5.7.3.6}topLeftCorner}{35}{subsubsection.5.7.3.6}
\contentsline {section}{\numberline {5.8}Paddle Class Reference}{36}{section.5.8}
\contentsline {subsection}{\numberline {5.8.1}Constructor \& Destructor Documentation}{37}{subsection.5.8.1}
\contentsline {subsubsection}{\numberline {5.8.1.1}Paddle}{37}{subsubsection.5.8.1.1}
\contentsline {subsection}{\numberline {5.8.2}Member Function Documentation}{37}{subsection.5.8.2}
\contentsline {subsubsection}{\numberline {5.8.2.1}BottomRight}{37}{subsubsection.5.8.2.1}
\contentsline {subsubsection}{\numberline {5.8.2.2}calcCornerPoints}{37}{subsubsection.5.8.2.2}
\contentsline {subsubsection}{\numberline {5.8.2.3}checkKey}{38}{subsubsection.5.8.2.3}
\contentsline {subsubsection}{\numberline {5.8.2.4}draw}{38}{subsubsection.5.8.2.4}
\contentsline {subsubsection}{\numberline {5.8.2.5}getBallPosition}{38}{subsubsection.5.8.2.5}
\contentsline {subsubsection}{\numberline {5.8.2.6}getPaddleSpeed}{38}{subsubsection.5.8.2.6}
\contentsline {subsubsection}{\numberline {5.8.2.7}getPos}{38}{subsubsection.5.8.2.7}
\contentsline {subsubsection}{\numberline {5.8.2.8}getSize}{39}{subsubsection.5.8.2.8}
\contentsline {subsubsection}{\numberline {5.8.2.9}move}{39}{subsubsection.5.8.2.9}
\contentsline {subsubsection}{\numberline {5.8.2.10}setDownFlag}{39}{subsubsection.5.8.2.10}
\contentsline {subsubsection}{\numberline {5.8.2.11}setUpFlag}{39}{subsubsection.5.8.2.11}
\contentsline {subsubsection}{\numberline {5.8.2.12}TopLeft}{39}{subsubsection.5.8.2.12}
\contentsline {subsection}{\numberline {5.8.3}Member Data Documentation}{40}{subsection.5.8.3}
\contentsline {subsubsection}{\numberline {5.8.3.1}bottomRightCorner}{40}{subsubsection.5.8.3.1}
\contentsline {subsubsection}{\numberline {5.8.3.2}origin}{40}{subsubsection.5.8.3.2}
\contentsline {subsubsection}{\numberline {5.8.3.3}paddleSpeed}{40}{subsubsection.5.8.3.3}
\contentsline {subsubsection}{\numberline {5.8.3.4}size}{40}{subsubsection.5.8.3.4}
\contentsline {subsubsection}{\numberline {5.8.3.5}topLeftCorner}{40}{subsubsection.5.8.3.5}
\contentsline {section}{\numberline {5.9}PlayerPaddle Class Reference}{41}{section.5.9}
\contentsline {subsection}{\numberline {5.9.1}Constructor \& Destructor Documentation}{42}{subsection.5.9.1}
\contentsline {subsubsection}{\numberline {5.9.1.1}PlayerPaddle}{42}{subsubsection.5.9.1.1}
\contentsline {subsection}{\numberline {5.9.2}Member Function Documentation}{42}{subsection.5.9.2}
\contentsline {subsubsection}{\numberline {5.9.2.1}BottomRight}{42}{subsubsection.5.9.2.1}
\contentsline {subsubsection}{\numberline {5.9.2.2}calcCornerPoints}{43}{subsubsection.5.9.2.2}
\contentsline {subsubsection}{\numberline {5.9.2.3}checkKey}{43}{subsubsection.5.9.2.3}
\contentsline {subsubsection}{\numberline {5.9.2.4}draw}{43}{subsubsection.5.9.2.4}
\contentsline {subsubsection}{\numberline {5.9.2.5}getBallPosition}{43}{subsubsection.5.9.2.5}
\contentsline {subsubsection}{\numberline {5.9.2.6}getPaddleSpeed}{43}{subsubsection.5.9.2.6}
\contentsline {subsubsection}{\numberline {5.9.2.7}getPos}{44}{subsubsection.5.9.2.7}
\contentsline {subsubsection}{\numberline {5.9.2.8}getSize}{44}{subsubsection.5.9.2.8}
\contentsline {subsubsection}{\numberline {5.9.2.9}move}{44}{subsubsection.5.9.2.9}
\contentsline {subsubsection}{\numberline {5.9.2.10}setDownFlag}{44}{subsubsection.5.9.2.10}
\contentsline {subsubsection}{\numberline {5.9.2.11}setUpFlag}{44}{subsubsection.5.9.2.11}
\contentsline {subsubsection}{\numberline {5.9.2.12}TopLeft}{45}{subsubsection.5.9.2.12}
\contentsline {subsection}{\numberline {5.9.3}Member Data Documentation}{45}{subsection.5.9.3}
\contentsline {subsubsection}{\numberline {5.9.3.1}bottomRightCorner}{45}{subsubsection.5.9.3.1}
\contentsline {subsubsection}{\numberline {5.9.3.2}DOWN\_\discretionary {-}{}{}ARROW\_\discretionary {-}{}{}PRESS}{45}{subsubsection.5.9.3.2}
\contentsline {subsubsection}{\numberline {5.9.3.3}DOWN\_\discretionary {-}{}{}KEY}{45}{subsubsection.5.9.3.3}
\contentsline {subsubsection}{\numberline {5.9.3.4}origin}{45}{subsubsection.5.9.3.4}
\contentsline {subsubsection}{\numberline {5.9.3.5}paddleSpeed}{45}{subsubsection.5.9.3.5}
\contentsline {subsubsection}{\numberline {5.9.3.6}size}{45}{subsubsection.5.9.3.6}
\contentsline {subsubsection}{\numberline {5.9.3.7}topLeftCorner}{45}{subsubsection.5.9.3.7}
\contentsline {subsubsection}{\numberline {5.9.3.8}UP\_\discretionary {-}{}{}ARROW\_\discretionary {-}{}{}PRESS}{45}{subsubsection.5.9.3.8}
\contentsline {subsubsection}{\numberline {5.9.3.9}UP\_\discretionary {-}{}{}KEY}{45}{subsubsection.5.9.3.9}
\contentsline {section}{\numberline {5.10}Point Class Reference}{46}{section.5.10}
\contentsline {subsection}{\numberline {5.10.1}Constructor \& Destructor Documentation}{46}{subsection.5.10.1}
\contentsline {subsubsection}{\numberline {5.10.1.1}Point}{46}{subsubsection.5.10.1.1}
\contentsline {subsection}{\numberline {5.10.2}Member Function Documentation}{46}{subsection.5.10.2}
\contentsline {subsubsection}{\numberline {5.10.2.1}operator+}{46}{subsubsection.5.10.2.1}
\contentsline {subsection}{\numberline {5.10.3}Member Data Documentation}{47}{subsection.5.10.3}
\contentsline {subsubsection}{\numberline {5.10.3.1}x}{47}{subsubsection.5.10.3.1}
\contentsline {subsubsection}{\numberline {5.10.3.2}y}{47}{subsubsection.5.10.3.2}
\contentsline {section}{\numberline {5.11}Score Class Reference}{48}{section.5.11}
\contentsline {subsection}{\numberline {5.11.1}Constructor \& Destructor Documentation}{49}{subsection.5.11.1}
\contentsline {subsubsection}{\numberline {5.11.1.1}Score}{49}{subsubsection.5.11.1.1}
\contentsline {subsection}{\numberline {5.11.2}Member Function Documentation}{49}{subsection.5.11.2}
\contentsline {subsubsection}{\numberline {5.11.2.1}draw}{49}{subsubsection.5.11.2.1}
\contentsline {subsubsection}{\numberline {5.11.2.2}getMaxScore}{50}{subsubsection.5.11.2.2}
\contentsline {subsubsection}{\numberline {5.11.2.3}getScore}{50}{subsubsection.5.11.2.3}
\contentsline {subsubsection}{\numberline {5.11.2.4}isMax}{50}{subsubsection.5.11.2.4}
\contentsline {subsubsection}{\numberline {5.11.2.5}move}{50}{subsubsection.5.11.2.5}
\contentsline {subsubsection}{\numberline {5.11.2.6}resetScore}{50}{subsubsection.5.11.2.6}
\contentsline {subsubsection}{\numberline {5.11.2.7}samplesLoaded}{51}{subsubsection.5.11.2.7}
\contentsline {subsubsection}{\numberline {5.11.2.8}scoreUp}{51}{subsubsection.5.11.2.8}
\contentsline {subsubsection}{\numberline {5.11.2.9}scoreUp2}{51}{subsubsection.5.11.2.9}
\contentsline {subsubsection}{\numberline {5.11.2.10}setMaxScore}{51}{subsubsection.5.11.2.10}
\contentsline {subsubsection}{\numberline {5.11.2.11}setScore}{52}{subsubsection.5.11.2.11}
\contentsline {subsection}{\numberline {5.11.3}Member Data Documentation}{52}{subsection.5.11.3}
\contentsline {subsubsection}{\numberline {5.11.3.1}bkgrnd\_\discretionary {-}{}{}music\_\discretionary {-}{}{}inst}{52}{subsubsection.5.11.3.1}
\contentsline {subsubsection}{\numberline {5.11.3.2}fon}{52}{subsubsection.5.11.3.2}
\contentsline {subsubsection}{\numberline {5.11.3.3}maxsc}{52}{subsubsection.5.11.3.3}
\contentsline {subsubsection}{\numberline {5.11.3.4}netTrigger}{52}{subsubsection.5.11.3.4}
\contentsline {subsubsection}{\numberline {5.11.3.5}p\_\discretionary {-}{}{}hit\_\discretionary {-}{}{}inst}{52}{subsubsection.5.11.3.5}
\contentsline {subsubsection}{\numberline {5.11.3.6}sc}{52}{subsubsection.5.11.3.6}
\contentsline {subsubsection}{\numberline {5.11.3.7}score\_\discretionary {-}{}{}inst}{52}{subsubsection.5.11.3.7}
\contentsline {subsubsection}{\numberline {5.11.3.8}w\_\discretionary {-}{}{}hit\_\discretionary {-}{}{}inst}{52}{subsubsection.5.11.3.8}
\contentsline {subsubsection}{\numberline {5.11.3.9}x}{53}{subsubsection.5.11.3.9}
\contentsline {subsubsection}{\numberline {5.11.3.10}y}{53}{subsubsection.5.11.3.10}
\contentsline {section}{\numberline {5.12}ScreenMaster Class Reference}{54}{section.5.12}
\contentsline {subsection}{\numberline {5.12.1}Member Function Documentation}{55}{subsection.5.12.1}
\contentsline {subsubsection}{\numberline {5.12.1.1}collision}{55}{subsubsection.5.12.1.1}
\contentsline {subsubsection}{\numberline {5.12.1.2}drawModel}{55}{subsubsection.5.12.1.2}
\contentsline {subsubsection}{\numberline {5.12.1.3}getQueue}{55}{subsubsection.5.12.1.3}
\contentsline {subsubsection}{\numberline {5.12.1.4}playerinput}{55}{subsubsection.5.12.1.4}
\contentsline {subsubsection}{\numberline {5.12.1.5}run}{55}{subsubsection.5.12.1.5}
\contentsline {subsubsection}{\numberline {5.12.1.6}samplesLoaded}{55}{subsubsection.5.12.1.6}
\contentsline {subsubsection}{\numberline {5.12.1.7}updateModel}{55}{subsubsection.5.12.1.7}
\contentsline {subsection}{\numberline {5.12.2}Member Data Documentation}{56}{subsection.5.12.2}
\contentsline {subsubsection}{\numberline {5.12.2.1}bkgrnd\_\discretionary {-}{}{}music\_\discretionary {-}{}{}inst}{56}{subsubsection.5.12.2.1}
\contentsline {subsubsection}{\numberline {5.12.2.2}fon}{56}{subsubsection.5.12.2.2}
\contentsline {subsubsection}{\numberline {5.12.2.3}gameOver}{56}{subsubsection.5.12.2.3}
\contentsline {subsubsection}{\numberline {5.12.2.4}height}{56}{subsubsection.5.12.2.4}
\contentsline {subsubsection}{\numberline {5.12.2.5}p\_\discretionary {-}{}{}hit\_\discretionary {-}{}{}inst}{56}{subsubsection.5.12.2.5}
\contentsline {subsubsection}{\numberline {5.12.2.6}score\_\discretionary {-}{}{}inst}{56}{subsubsection.5.12.2.6}
\contentsline {subsubsection}{\numberline {5.12.2.7}w\_\discretionary {-}{}{}hit\_\discretionary {-}{}{}inst}{56}{subsubsection.5.12.2.7}
\contentsline {subsubsection}{\numberline {5.12.2.8}width}{56}{subsubsection.5.12.2.8}
\contentsline {section}{\numberline {5.13}Simulator Class Reference}{57}{section.5.13}
\contentsline {subsection}{\numberline {5.13.1}Detailed Description}{57}{subsection.5.13.1}
\contentsline {subsection}{\numberline {5.13.2}Constructor \& Destructor Documentation}{58}{subsection.5.13.2}
\contentsline {subsubsection}{\numberline {5.13.2.1}Simulator}{58}{subsubsection.5.13.2.1}
\contentsline {subsubsection}{\numberline {5.13.2.2}$\sim $Simulator}{58}{subsubsection.5.13.2.2}
\contentsline {subsection}{\numberline {5.13.3}Member Function Documentation}{58}{subsection.5.13.3}
\contentsline {subsubsection}{\numberline {5.13.3.1}collision}{58}{subsubsection.5.13.3.1}
\contentsline {subsubsection}{\numberline {5.13.3.2}drawModel}{58}{subsubsection.5.13.3.2}
\contentsline {subsubsection}{\numberline {5.13.3.3}getQueue}{58}{subsubsection.5.13.3.3}
\contentsline {subsubsection}{\numberline {5.13.3.4}playerinput}{58}{subsubsection.5.13.3.4}
\contentsline {subsubsection}{\numberline {5.13.3.5}run}{58}{subsubsection.5.13.3.5}
\contentsline {subsubsection}{\numberline {5.13.3.6}updateModel}{58}{subsubsection.5.13.3.6}
\contentsline {subsection}{\numberline {5.13.4}Member Data Documentation}{59}{subsection.5.13.4}
\contentsline {subsubsection}{\numberline {5.13.4.1}eventQueue}{59}{subsubsection.5.13.4.1}
\contentsline {subsubsection}{\numberline {5.13.4.2}framesPerSec}{59}{subsubsection.5.13.4.2}
\contentsline {subsubsection}{\numberline {5.13.4.3}gameOver}{59}{subsubsection.5.13.4.3}
\contentsline {subsubsection}{\numberline {5.13.4.4}timer}{59}{subsubsection.5.13.4.4}
\contentsline {section}{\numberline {5.14}Tests Class Reference}{60}{section.5.14}
\contentsline {subsection}{\numberline {5.14.1}Member Function Documentation}{61}{subsection.5.14.1}
\contentsline {subsubsection}{\numberline {5.14.1.1}CPPUNIT\_\discretionary {-}{}{}TEST}{61}{subsubsection.5.14.1.1}
\contentsline {subsubsection}{\numberline {5.14.1.2}CPPUNIT\_\discretionary {-}{}{}TEST}{61}{subsubsection.5.14.1.2}
\contentsline {subsubsection}{\numberline {5.14.1.3}CPPUNIT\_\discretionary {-}{}{}TEST}{61}{subsubsection.5.14.1.3}
\contentsline {subsubsection}{\numberline {5.14.1.4}CPPUNIT\_\discretionary {-}{}{}TEST}{61}{subsubsection.5.14.1.4}
\contentsline {subsubsection}{\numberline {5.14.1.5}CPPUNIT\_\discretionary {-}{}{}TEST}{61}{subsubsection.5.14.1.5}
\contentsline {subsubsection}{\numberline {5.14.1.6}CPPUNIT\_\discretionary {-}{}{}TEST}{61}{subsubsection.5.14.1.6}
\contentsline {subsubsection}{\numberline {5.14.1.7}CPPUNIT\_\discretionary {-}{}{}TEST}{61}{subsubsection.5.14.1.7}
\contentsline {subsubsection}{\numberline {5.14.1.8}CPPUNIT\_\discretionary {-}{}{}TEST\_\discretionary {-}{}{}SUITE}{61}{subsubsection.5.14.1.8}
\contentsline {subsubsection}{\numberline {5.14.1.9}CPPUNIT\_\discretionary {-}{}{}TEST\_\discretionary {-}{}{}SUITE\_\discretionary {-}{}{}END}{61}{subsubsection.5.14.1.9}
\contentsline {subsubsection}{\numberline {5.14.1.10}sampleLoadTest}{61}{subsubsection.5.14.1.10}
\contentsline {subsubsection}{\numberline {5.14.1.11}setUp}{61}{subsubsection.5.14.1.11}
\contentsline {subsubsection}{\numberline {5.14.1.12}tearDown}{61}{subsubsection.5.14.1.12}
\contentsline {subsubsection}{\numberline {5.14.1.13}testHorzBounce}{61}{subsubsection.5.14.1.13}
\contentsline {subsubsection}{\numberline {5.14.1.14}testMove}{61}{subsubsection.5.14.1.14}
\contentsline {subsubsection}{\numberline {5.14.1.15}testResetBall}{61}{subsubsection.5.14.1.15}
\contentsline {subsubsection}{\numberline {5.14.1.16}testScore}{61}{subsubsection.5.14.1.16}
\contentsline {subsubsection}{\numberline {5.14.1.17}testVerticalBottom}{61}{subsubsection.5.14.1.17}
\contentsline {subsubsection}{\numberline {5.14.1.18}testVerticalTop}{61}{subsubsection.5.14.1.18}
\contentsline {subsection}{\numberline {5.14.2}Member Data Documentation}{61}{subsection.5.14.2}
\contentsline {subsubsection}{\numberline {5.14.2.1}a}{61}{subsubsection.5.14.2.1}
\contentsline {subsubsection}{\numberline {5.14.2.2}b}{61}{subsubsection.5.14.2.2}
\contentsline {subsubsection}{\numberline {5.14.2.3}b2}{61}{subsubsection.5.14.2.3}
\contentsline {subsubsection}{\numberline {5.14.2.4}p}{61}{subsubsection.5.14.2.4}
\contentsline {subsubsection}{\numberline {5.14.2.5}s}{61}{subsubsection.5.14.2.5}
\contentsline {section}{\numberline {5.15}Updateable Class Reference}{62}{section.5.15}
\contentsline {subsection}{\numberline {5.15.1}Member Function Documentation}{62}{subsection.5.15.1}
\contentsline {subsubsection}{\numberline {5.15.1.1}move}{62}{subsubsection.5.15.1.1}
\contentsline {section}{\numberline {5.16}Vector Class Reference}{63}{section.5.16}
\contentsline {subsection}{\numberline {5.16.1}Constructor \& Destructor Documentation}{63}{subsection.5.16.1}
\contentsline {subsubsection}{\numberline {5.16.1.1}Vector}{63}{subsubsection.5.16.1.1}
\contentsline {subsection}{\numberline {5.16.2}Member Function Documentation}{64}{subsection.5.16.2}
\contentsline {subsubsection}{\numberline {5.16.2.1}operator$\ast $}{64}{subsubsection.5.16.2.1}
\contentsline {subsubsection}{\numberline {5.16.2.2}operator+}{64}{subsubsection.5.16.2.2}
\contentsline {subsubsection}{\numberline {5.16.2.3}operator==}{64}{subsubsection.5.16.2.3}
\contentsline {subsubsection}{\numberline {5.16.2.4}randomVector}{64}{subsubsection.5.16.2.4}
\contentsline {subsection}{\numberline {5.16.3}Member Data Documentation}{65}{subsection.5.16.3}
\contentsline {subsubsection}{\numberline {5.16.3.1}x}{65}{subsubsection.5.16.3.1}
\contentsline {subsubsection}{\numberline {5.16.3.2}y}{65}{subsubsection.5.16.3.2}
\contentsline {chapter}{\numberline {6}File Documentation}{67}{chapter.6}
\contentsline {section}{\numberline {6.1}Audio.cc File Reference}{67}{section.6.1}
\contentsline {section}{\numberline {6.2}Audio.h File Reference}{68}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Detailed Description}{68}{subsection.6.2.1}
\contentsline {section}{\numberline {6.3}Ball.cc File Reference}{69}{section.6.3}
\contentsline {section}{\numberline {6.4}Ball.h File Reference}{70}{section.6.4}
\contentsline {subsection}{\numberline {6.4.1}Detailed Description}{70}{subsection.6.4.1}
\contentsline {section}{\numberline {6.5}ComputerPaddle.cc File Reference}{71}{section.6.5}
\contentsline {section}{\numberline {6.6}ComputerPaddle.h File Reference}{72}{section.6.6}
\contentsline {subsection}{\numberline {6.6.1}Detailed Description}{72}{subsection.6.6.1}
\contentsline {section}{\numberline {6.7}Display.cc File Reference}{73}{section.6.7}
\contentsline {section}{\numberline {6.8}Display.h File Reference}{74}{section.6.8}
\contentsline {section}{\numberline {6.9}Drawable.h File Reference}{75}{section.6.9}
\contentsline {subsection}{\numberline {6.9.1}Detailed Description}{75}{subsection.6.9.1}
\contentsline {section}{\numberline {6.10}main-\/game.cc File Reference}{76}{section.6.10}
\contentsline {subsection}{\numberline {6.10.1}Detailed Description}{76}{subsection.6.10.1}
\contentsline {subsection}{\numberline {6.10.2}Function Documentation}{76}{subsection.6.10.2}
\contentsline {subsubsection}{\numberline {6.10.2.1}main}{76}{subsubsection.6.10.2.1}
\contentsline {section}{\numberline {6.11}MenuMaster.cc File Reference}{77}{section.6.11}
\contentsline {section}{\numberline {6.12}MenuMaster.h File Reference}{78}{section.6.12}
\contentsline {subsection}{\numberline {6.12.1}Detailed Description}{78}{subsection.6.12.1}
\contentsline {section}{\numberline {6.13}Net.h File Reference}{79}{section.6.13}
\contentsline {subsection}{\numberline {6.13.1}Detailed Description}{79}{subsection.6.13.1}
\contentsline {section}{\numberline {6.14}Paddle.cc File Reference}{80}{section.6.14}
\contentsline {section}{\numberline {6.15}Paddle.h File Reference}{81}{section.6.15}
\contentsline {subsection}{\numberline {6.15.1}Detailed Description}{81}{subsection.6.15.1}
\contentsline {section}{\numberline {6.16}PlayerPaddle.cc File Reference}{82}{section.6.16}
\contentsline {section}{\numberline {6.17}PlayerPaddle.h File Reference}{83}{section.6.17}
\contentsline {subsection}{\numberline {6.17.1}Detailed Description}{83}{subsection.6.17.1}
\contentsline {section}{\numberline {6.18}Point.h File Reference}{84}{section.6.18}
\contentsline {subsection}{\numberline {6.18.1}Detailed Description}{84}{subsection.6.18.1}
\contentsline {section}{\numberline {6.19}Score.cc File Reference}{85}{section.6.19}
\contentsline {section}{\numberline {6.20}Score.h File Reference}{86}{section.6.20}
\contentsline {subsection}{\numberline {6.20.1}Detailed Description}{86}{subsection.6.20.1}
\contentsline {section}{\numberline {6.21}ScreenMaster.cc File Reference}{87}{section.6.21}
\contentsline {section}{\numberline {6.22}ScreenMaster.h File Reference}{88}{section.6.22}
\contentsline {subsection}{\numberline {6.22.1}Detailed Description}{88}{subsection.6.22.1}
\contentsline {section}{\numberline {6.23}Simulator.cc File Reference}{89}{section.6.23}
\contentsline {section}{\numberline {6.24}Simulator.h File Reference}{90}{section.6.24}
\contentsline {section}{\numberline {6.25}testpong.cc File Reference}{91}{section.6.25}
\contentsline {subsection}{\numberline {6.25.1}Function Documentation}{91}{subsection.6.25.1}
\contentsline {subsubsection}{\numberline {6.25.1.1}main}{91}{subsubsection.6.25.1.1}
\contentsline {section}{\numberline {6.26}tests.h File Reference}{92}{section.6.26}
\contentsline {section}{\numberline {6.27}Updateable.h File Reference}{93}{section.6.27}
\contentsline {subsection}{\numberline {6.27.1}Detailed Description}{93}{subsection.6.27.1}
\contentsline {section}{\numberline {6.28}Vector.h File Reference}{94}{section.6.28}
\contentsline {subsection}{\numberline {6.28.1}Detailed Description}{94}{subsection.6.28.1}
