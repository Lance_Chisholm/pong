/**
 *@file Point.h
 *@brief declaration of the point class.
 *@details this class exists so that we can define the middle/origin points of our objects
 * on screen.
 *@author Lance Chisholm, Eric DenHaan, Shaun Cullen, Joshua Vandenhoek.
 *@bug no known bugs.
 */

#ifndef POINT_H
#define POINT_H

#include "Vector.h"

class Point
{
  public:
   float x,y; /**< data members that represent the x and y coordinates of a point. */

   /**
    *@fn Point(float a = 0.0, float b=0.0):x(a), y(b)
    *@brief Constructor for point objects.
    *@details Constructor for the point objects that takes 2 input arguments and initializes
    *the private data members accordingly.
    *@param a float value that initializes the x private data member
    *@param b float value that initializes the y private data member.
    *@return Point object.
    */
    Point(float a = 0.0, float b=0.0):x(a), y(b) {};



   /**
    *@fn Point operator + (Vector v)
    *@brief overloaded + operator.
    *@details This overloaded + operator is designed to take a vector object as an argument and change the position
    * of a point based on the vector's direction.
    *@param V Vector object that will change the position of the point object.
    *@return Point object with a changed position based on the input vector.
    */
    Point operator + (Vector v){
   return Point(x+v.x, y+v.y);

   
   }
};
#endif // POINT_H
