// CPSC 2720
// Group Project
// Audio.h
// Class which initializes and deallocates all audio components (interface)
// Written By: Eric Den Haan

/**
 *@file Audio.h
 *@brief Declaration of the audio class.
 *@details This class is responsible for handling the objects necessary for sound to be played during the game.
 *There are various sounds in the game, including background music, wall hits, paddle hits, and scoring.
 *@author Eric DenHaan
 *@bug no known bugs.
 */

#ifndef AUDIO_H
#define AUDIO_H

#include <allegro5/allegro.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <cstdlib>
#include <string>

class Audio
{
  private:
   // the sound samples

   // background music
   ALLEGRO_SAMPLE *bkgrnd_music; /**< private data member that is a pointer to an allegro sample. This points to the background music sound sample.*/  

   // wall hit sound effect
   ALLEGRO_SAMPLE *w_hit; /**< private data member that is a pointer to an allegro sample. This points to the wall hit sound sample.*/

   // paddle hit sound effect
   ALLEGRO_SAMPLE *p_hit; /**< private data member that is a pointer to an allegro sample. This points to the paddle hit sound sample.*/

   // score sound effect
   ALLEGRO_SAMPLE *score; /**< private data member that is a pointer to an allegro sample. This points to the score sound sample.*/

  protected:
   // the sample instances, which will be played in ball and score

   ALLEGRO_SAMPLE_INSTANCE *bkgrnd_music_inst; /**< protected member that is a pointer to an allegro sample instance. This points to a background music instance.*/

   ALLEGRO_SAMPLE_INSTANCE *w_hit_inst; /**< protected member that is a pointer to an allegro sample instance. This points to a wall hit instance.*/

   ALLEGRO_SAMPLE_INSTANCE *p_hit_inst; /**< protected member that is a pointer to an allegro sample instance. This points to a paddle hit instance.*/

   ALLEGRO_SAMPLE_INSTANCE *score_inst; /**< protected member that is a pointer to an allegro sample instance. This points to a score instance.*/
  
  public:

/**
 *@fn Audio()
 *@brief Constructor for the Audio objects.
 *@details This constructor installs the necessary audio codec, reserves space for the number of samples that you want, and then loads the samples.
 *Instances of the sounds are also created, and the play mode for the background music is established. Each of the instances are also attached to the mixer.
 *@param none.
 *@return Audio object.
 */
// Constructor, initializes audio components
   Audio();

/**
 *@fn ~Audio.h()
 *@brief destructor for the audio objects.
 *@details releases all of the memory held by the sample instances and the samples.
 *@param none.
 *@return released memory.
 */
// Destructor, releases audio resources
   ~Audio();
   /**
    *@fn bool samplesLoaded()
    *@brief function that tests whether or not the audio samples have loaded.
    *@details function that tests whether or not the audio samples have loaded. Checks to see that the sample pointers are not NULL.
    *@param none.
    *@return true/false.
    */
// checks to see if samples have loaded, for testing purposes
   bool samplesLoaded();
};

#endif
   
