/**
 *@file main-game.cc
 *@brief Runs the game.
 *@details The file were the executable comes from.
 *@authour Lance Chisholm, Eric DenHaan, Shaun Cullen, Joushua Vandenhoek.
 *@bug lots.
 */
#include "ScreenMaster.h"
#include "MenuMaster.h"
#include <ctime>
#include <cstdlib>
#include <random>

int main ()
{
   bool continueGame=true;/**<a bool.*/
   int mode;/**<Represents the mode.*/
   //random seed for our random numbers
   srand (time(NULL));

   //this initializes a Display object of size 800 x 600
   Display displayobject(800,600);/**<the actual display the game will use.*/
   Audio audioobject;/**<The audio for the game.*/
   al_init_font_addon();
   al_init_ttf_addon();

   MenuMaster menu(displayobject,audioobject,100);/**<The menu for the game.*/

   mode = menu.runMenu();
 
   //initialize the game ScreenMaster object with the display and set
   //the fps to 100
   ScreenMaster game(displayobject, audioobject, 100);
 
 if (mode == 1)
 {
   game.initialize();
   game.run();
   mode = 0;
 }
 else if (mode ==2)
 {
   game.initialize3();
   game.run();
   mode =0;
 }
 std::cout << "Mode is: "<< mode << std::endl;  
//game.initialize2();
}






  
